package kh.edu.bbu;

import kh.edu.bbu.models.Teacher;
//import kh.edu.bbu.service.TeacherService;
import kh.edu.bbu.service.impl.TeacherServiceImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        TeacherServiceImpl teacherService = new TeacherServiceImpl();
        Scanner input = new Scanner(System.in);
        int choice;
        do{

            Teacher teacher = new Teacher();
            teacherService.menuOption();
            choice = input.nextInt();
            int id;
            switch (choice){
                case 1:
                    System.out.println("\nALL INFORMATION LIST DATA");
                    teacherService.getAllTeacherWithTableGenerator(teacherService.getAllTeacher());
                    break;
                case 2:
                    System.out.println("Form Add new Teacher");
                    System.out.print("ID : ");
                    teacher.setId(input.next());
                    System.out.print("FIRST NAME : ");
                    teacher.setFirstName(input.next());
                    System.out.print("LAST NAME : ");
                    teacher.setLastName(input.next());
                    System.out.print("GENDER : ");
                    teacher.setGender(input.next());
                    System.out.print("PHONE : ");
                    teacher.setPhone(input.next());
                    System.out.print("ADDRESS : ");
                    teacher.setAddress(input.next());
                    System.out.print("DATE OF BIRTH : ");
                    teacher.setDateOfBirth(input.next());
                    System.out.print("UNIVERSITY : ");
                    teacher.setUniversity(input.next());
                    System.out.print("CLASS AMOUNT : ");
                    teacher.setClassAmount(input.next());
                    System.out.print("TEACH PERIOD : ");
                    teacher.setTeachPeriod(input.next());
                    System.out.print("SUBJECT : ");
                    teacher.setSubject(input.next());

                    teacherService.addNeTeacher(teacher);
                    System.out.println("New Record Added");
                    break;
                case 3:
                    System.out.println("Form Update Teacher");
                    System.out.print("ID : ");
                    teacher.setId(input.next());
                    System.out.print("FIRST NAME : ");
                    teacher.setFirstName(input.next());
                    System.out.print("LAST NAME : ");
                    teacher.setLastName(input.next());
                    System.out.print("GENDER : ");
                    teacher.setGender(input.next());
                    System.out.print("PHONE : ");
                    teacher.setPhone(input.next());
                    System.out.print("ADDRESS : ");
                    teacher.setAddress(input.next());

                    System.out.print("DATE OF BIRTH : ");
                    teacher.setDateOfBirth(input.next());
                    System.out.print("UNIVERSITY : ");
                    teacher.setUniversity(input.next());
                    System.out.print("CLASS AMOUNT : ");
                    teacher.setClassAmount(input.next());
                    System.out.print("TEACH PERI0D : ");
                    teacher.setTeachPeriod(input.next());
                    System.out.print("SUBJECT : ");
                    teacher.setSubject(input.next());

                    teacherService.updateTeacher(teacher);
                    System.out.println("Update Success");
                    break;
                case 4:
                    System.out.println("\nForm Find Teacher");
                    System.out.print("ID : ");
                    teacher.setId(input.next());
                    teacherService.FindById(teacher.getId());
                    break;

                case 5:
                    System.out.println("Form Delete Teacher");
                    System.out.print("ID : ");
                    teacher.setId(input.next());
                    teacherService.deleteTeacher(teacher.getId());
                    System.out.println("Delete success");
                    break;
                case 6:
                    choice = 0;
                    break;
            }
        }while (choice !=0);

    }

}
