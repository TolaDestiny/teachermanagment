package kh.edu.bbu.service.impl;

import kh.edu.bbu.models.Teacher;
import kh.edu.bbu.service.TeacherService;
import kh.edu.bbu.utils.TableGenerator;
import sun.awt.windows.WSystemTrayPeer;

//import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class TeacherServiceImpl implements TeacherService {
    List<Teacher> teacherList = new ArrayList<>();

    @Override
    public void addNeTeacher(Teacher teacher) {

            teacherList.add(teacher);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        for(Teacher stud: teacherList){
            if(stud.getId().equals(teacher.getId())) {
                teacherList.set(teacherList.indexOf(stud), teacher);
                break;
            }
        }
    }

    @Override
    public void deleteTeacher(String id) {
        for(Teacher stud: teacherList){
            if(stud.getId().equals(id)) {
                teacherList.remove(stud);
                break;
            }
        }
    }

    @Override
    public void FindById(String id) {

       for(Teacher teacher: teacherList){
            if(teacher.getId().equals(id)){
                //studentList.remove(stud);
                System.out.print("\n"+teacher.getId()+"\t\t"+teacher.getFirstName() +"\t\t"+teacher.getLastName()+"\t\t"+teacher.getGender()+"\t\t"+teacher.getPhone());
                System.out.print("\t\t"+teacher.getAddress()+"\t\t"+teacher.getDateOfBirth()+"\t\t"+teacher.getUniversity());
                System.out.println("\t\t"+teacher.getClassAmount()+"\t\t"+teacher.getTeachPeriod()+"\t\t"+teacher.getSubject()+"\n");
            }
        }
        }

    @Override
    public List<Teacher> getAllTeacher() {
        return teacherList;
    }

    @Override
    public void getAllTeacherWithTableGenerator(List<Teacher> list) {
        TableGenerator tableGenerator = new TableGenerator();
        List<String> headerList = new ArrayList<>();
        headerList.add("ID");
        headerList.add("First Name");
        headerList.add("Last Name");
        headerList.add("Gender");
        headerList.add("Phone");
        headerList.add("Address");
        headerList.add("DateOfBirth");
        headerList.add("University");
        headerList.add("ClassAmount");
        headerList.add("TeachPeriod");
        headerList.add("Subject");

        List<List<String>> rowsList = new ArrayList<>();
        list.forEach(c->{
            List<String> rows = new ArrayList<>();
            rows.add(c.getId());
            rows.add(c.getFirstName());
            rows.add(c.getLastName());
            rows.add(c.getGender());
            rows.add(c.getPhone());
            rows.add(c.getAddress());
            rows.add(c.getDateOfBirth());
            rows.add(c.getUniversity());
            rows.add(c.getClassAmount());
            rows.add(c.getTeachPeriod());
            rows.add(c.getSubject());
            rowsList.add(rows);
        });
        System.out.println(tableGenerator.generateTable(headerList,rowsList));
    }

    @Override
    public void menuOption() {
        System.out.println("List->(1) || Add New->(2) || Update->(3) || FindById->(4) || Delete->(5) || Exit->(6)");
    }
}
